<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use SoftDeletes;

    protected $fillable = [
    	'user_id',
    	'order_id',
		'customer_id_stripe',
		'id_stripe',
		'card_fingerprint',
		'status',
		'name',
        'card_digit',
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function order()
    {
    	return $this->belongsTo('App\Order');
    }
}
