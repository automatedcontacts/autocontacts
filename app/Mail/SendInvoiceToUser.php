<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendInvoiceToUser extends Mailable
{
    use Queueable, SerializesModels;

    public $order;
    public $profile;
    public $add_ons;
    public $fuel_detail;
    public $payment;
    public $user;
    public $card_number;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order, $profile, $add_ons, $fuel_detail, $payment, $user,$last4)
    {
        $this->order = $order;
        $this->profile = $profile;
        $this->add_ons = $add_ons;
        $this->fuel_detail = $fuel_detail;
        $this->payment = $payment;
        $this->user = $user;
        $this->card_number = $last4;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $id = $this->order->id;
        return $this->subject("Thank you for your Meadows Petroleum Order # $id")->view('emails.invoice_to_user');
    }
}
