<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TankDetail extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'tank_location', 'tank_capacity', 'current_status',
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
