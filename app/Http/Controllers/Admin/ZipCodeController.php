<?php

namespace App\Http\Controllers\Admin;

use App\{ ZipCode };
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ZipCodeController extends Controller
{
    public function __construct(ZipCode $zip_code)
   	{
   		$this->zip_code = $zip_code;
   	}
    
    /**
     * Function to display all Zip Code available
     * 
     * @return [view] admin.zip_code.list
     */
    public function zipCodeList()
   	{
   		try {
   			$zip_codes = $this->zip_code->latest('created_at')->get();
   			
   			return view('admin.zip_code.list', compact('zip_codes'));
   		} catch (\Exception $e) {
   			dd($e);
   		}
   	}

   	/**
   	 * Function for adding Zip Code into database
   	 * 
   	 * @param Request
   	 * @return [redirect] back to save route
   	 */
   	public function addZipCode(Request $request)
   	{
   		try {
   			$this->zip_code->create([
   				'name'		=> $request->name,
   				'code' 	 	=> $request->code,
   			]);

   			flash()->success('New Record Added', 'New Zip Code Hasbeen Added');
   			return redirect()->back();
   		} catch (\Exception $e) {
   			dd($e);
   		}
   	}

   	/**
   	 * Function to display zip code edit page
   	 * 
   	 * @param  [id] $id must be int
   	 * @return [view] admin.zip_code.edit
   	 */
   	public function editZipCode(int $id)
   	{
   		try {
   			$data = $this->zip_code->where('id', $id)->first();

   			if (empty($data)) {
   				flash()->info('OOPS!', 'Try Again with valide ID');
   				return redirect()->route('admin.zip.code.list');
   			}

   			return view('admin.zip_code.edit', compact('data'));
   		} catch (\Exception $e) {
   			dd($e);
   		}
   	}

   	/** 
   	 * Function to update the Requested zip code
   	 * 
   	 * @param  Request
   	 * @return [redirect] route('admin.zip.code.list')
   	 */
   	public function updateZipCode(Request $request)
   	{
   		try {
   			$data = $this->zip_code->where('id', $request->id)->first();

   			if (empty($data)) {
   				flash()->info('OOPS!', 'Try Again with valide ID');
   				return redirect()->route('admin.zip.code.list');
   			}

   			$data->update([
   				'name'		=> $request->name,
   				'code' 	 	=> $request->code,
   			]);

   			flash()->success('Record Updated', 'Zip Code Hasbeen updated');
   			return redirect()->route('admin.zip.code.list');
   		} catch (\Exception $e) {
   			dd($e);
   		}
   	}
}
