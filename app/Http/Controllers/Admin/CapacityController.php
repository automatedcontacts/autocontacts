<?php
namespace App\Http\Controllers\Admin;

use App\{ FuelRate, ZipCode, TankCapacity};
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class CapacityController extends Controller
{
	public function index(){
       $tank_capacity=TankCapacity::all();
       return view('admin.capacity.list_capacity',compact('tank_capacity'));
	}
	public function create(Request $request){
		try {
   			TankCapacity::create([
   				'capacity'		=> $request->capacity,
   			]);
            return redirect()->back();;
   		} catch (\Exception $e) {
   			dd($e);
   		}
	}
	public function edit(int $id)
	{
		try {
			$data = TankCapacity::where('id', $id)->first();
			if (empty($data)) {
				flash()->info('OOPS!', 'Try Again with valide ID');
				return redirect()->route('admin.services.list');
			}
         return view('admin.capacity.edit', compact('data'));
		} catch (\Exception $e) {
			dd($e);
		}
	}
	public function update(Request $request){
		try {
			$data = TankCapacity::where('id', $request->id)->first();

			if (empty($data)) {
				flash()->info('OOPS!', 'Try Again with valide ID');
				return redirect()->route('admin.tank.capacity.list');
			}

			$data->update([
				'capacity'		=> $request->capacity,
			]);

			return redirect()->route('admin.tank.capacity.list');
		} catch (\Exception $e) {
			dd($e);
		}
	}
}
?>