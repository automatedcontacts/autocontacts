<?php

namespace App\Http\Controllers\Admin;

use App\{ FuelRate, ZipCode };
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FuelController extends Controller
{
    public function __construct(FuelRate $fuel, ZipCode $zip_code)
   	{
   		$this->zip_code = $zip_code;
   		$this->fuel = $fuel;
   	}
    
    /**
     * Function to display all fuel Rate available
     * 
     * @return [view] admin.services.list
     */
    public function fuelsList()
   	{
   		try {
   			$fuels = $this->fuel->with('zipCode')->latest('created_at')->get();
   			$zip_codes = $this->getZipCodeArray($this->zip_code->latest('created_at')->get());
   			
   			return view('admin.fuels.list', compact('fuels', 'zip_codes'));
   		} catch (\Exception $e) {
   			dd($e);
   		}
   	}

   	/**
   	 * Function for adding fuel rate into database
   	 * 
   	 * @param Request
   	 * @return [redirect] back to save route
   	 */
   	public function addFuel(Request $request)
   	{
   		try {
   			$zip_code_id = $request->zip_code_id == 0 ? $zip_code_id = null : $zip_code_id = $request->zip_code_id;
   			$is_global = $request->zip_code_id == 0 ? $is_global = 1 : $is_global = null;

   			$this->fuel->create([
   				'zip_code_id'		=> $zip_code_id,
   				'rate_per_gallon' 	=> $request->rate_per_gallon,
   				'is_global'			=> $is_global,
   			]);

   			flash()->success('New Record Added', 'New Fuel Record Hasbeen Added');
   			return redirect()->back();
   		} catch (\Exception $e) {
   			dd($e);
   		}
   	}

   	/**
   	 * Function to display fuel rate edit page
   	 * 
   	 * @param  [id] $id must be int
   	 * @return [view] admin.fuel.edit
   	 */
   	public function editFuel(int $id)
   	{
   		try {
   			$data = $this->fuel->where('id', $id)->first();

   			if (empty($data)) {
   				flash()->info('OOPS!', 'Try Again with valide ID');
   				return redirect()->route('admin.fuel.list');
   			}

   			$zip_codes = $this->getZipCodeArray($this->zip_code->latest('created_at')->get());

   			return view('admin.fuels.edit', compact('data', 'zip_codes'));
   		} catch (\Exception $e) {
   			dd($e);
   		}
   	}

   	/** 
   	 * Function to update the Requested Fuel Rate
   	 * 
   	 * @param  Request
   	 * @return [redirect] route('admin.fuel.list')
   	 */
   	public function updateFuel(Request $request)
   	{
   		try {
            
   			$data = $this->fuel->where('id', $request->id)->first();

   			if (empty($data)) {
   				flash()->info('OOPS!', 'Try Again with valide ID');
   				return redirect()->route('admin.fuel.list');
   			}

   			$zip_code = $request->zip_code_id == 0 ? $zip_code = null : $zip_code = $request->zip_code_id;
   			$is_global = $request->zip_code_id == 0 ? $is_global = 1 : $is_global = null;
   			$data->update([
   				'zip_code_id'		=> $zip_code,
   				'rate_per_gallon' => $request->rate_per_gallon,
   				'is_global'			=> $is_global,
   			]);

   			flash()->success('Record Updated', 'Fuel Record Hasbeen updated');
   			return redirect()->route('admin.fuel.list');
   		} catch (\Exception $e) {
   			dd($e);
   		}
   	}
}
