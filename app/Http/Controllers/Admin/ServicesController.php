<?php

namespace App\Http\Controllers\Admin;

use App\{ AdditionalService };
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServicesController extends Controller
{
	public function __construct(AdditionalService $services)
   	{
   		$this->services = $services;
   	}
    
    /**
     * Function to display all service available
     * 
     * @return [view] admin.services.list
     */
    public function servicesList()
   	{
   		try {
   			$services = $this->services->latest('created_at')->get();

   			return view('admin.services.list', compact('services'));
   		} catch (\Exception $e) {
   			dd($e);
   		}
   	}

   	/**
   	 * Function for adding service into database
   	 * 
   	 * @param Request
   	 * @return [redirect] back to save route
   	 */
   	public function addService(Request $request)
   	{
   		try {
   			$this->services->create([
   				'value'		=> $request->value,
   				'price'		=> $request->price,
   			]);

   			return redirect()->back();
   		} catch (\Exception $e) {
   			dd($e);
   		}
   	}

   	/**
   	 * Function to display service edit page
   	 * 
   	 * @param  [id] $id must be int
   	 * @return [view] admin.services.edit
   	 */
   	public function editService(int $id)
   	{
   		try {
   			$data = $this->services->where('id', $id)->first();

   			if (empty($data)) {
   				flash()->info('OOPS!', 'Try Again with valide ID');
   				return redirect()->route('admin.services.list');
   			}

   			return view('admin.services.edit', compact('data'));
   		} catch (\Exception $e) {
   			dd($e);
   		}
   	}

   	/** 
   	 * Function to update the Requested Service
   	 * 
   	 * @param  Request
   	 * @return [redirect] route('admin.services.list')
   	 */
   	public function updateService(Request $request)
   	{
   		try {
   			$data = $this->services->where('id', $request->id)->first();

   			if (empty($data)) {
   				flash()->info('OOPS!', 'Try Again with valide ID');
   				return redirect()->route('admin.services.list');
   			}

   			$data->update([
   				'value'		=> $request->value,
   				'price'		=> $request->price
   			]);

   			return redirect()->route('admin.services.list');
   		} catch (\Exception $e) {
   			dd($e);
   		}
   	}
}