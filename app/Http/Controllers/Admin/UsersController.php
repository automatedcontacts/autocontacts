<?php

namespace App\Http\Controllers\Admin;

use App\{ User, ZipCode, Order, Payment };
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use App\http\Controllers\Auth\RegisterController;

class UsersController extends Controller
{
    public function __construct(User $user, ZipCode $zip_code, Order $order, Payment $payment)
   	{
   		$this->user = $user;
        $this->order = $order;
        $this->payment = $payment;
   		$this->zip_code = $zip_code;
   	}
   	
   	/**
     * Function to display all users available
     * 
     * @return [view] admin.users.list
     */
   	public function usersList()
   	{
   		try {
   			$users = $this->user->latest('created_at')->get();
   			$zip_codes = $this->getZipCodeArray($this->zip_code->latest('created_at')->get(), true);

   			return view('admin.users.list', compact('users', 'zip_codes'));
   		} catch (\Exception $e) {
   			dd($e);
   		}
   	}

   	/**
   	 * Function for adding users into database
   	 * 
   	 * @param LoginRequest
   	 * @return [redirect] back to save route
   	 */
   	public function addUser(LoginRequest $request)
   	{
   		try {
   			$zip_code = $this->zip_code->where('id', $request->zip_code)->first();

   			if (empty($zip_code)) {
   				flash()->info('OOPS', 'Zip Code is not valid');
   				return redirect()->back();
   			}

   			$user = $this->checkUserStatus($request->email, $zip_code->code, true);

   			if (!$user) {
	            flash()->error('OOPS!', 'Internal Server Error.');
	            return redirect()->back()->withInput($request->all());;
	        }

	        if ($user == 'blocked') {
	            flash()->info('OOPS!', 'Not Authorized To Perform This Action.');
	            return redirect()->back()->withInput($request->all());;
	        }

	        if ($user == 'not found') {
	            $user = RegisterController::create([
	   				'name'				=> $request->name,
	   				'email'	 			=> $request->email,
	   				'zip_code'			=> $zip_code->code,
	   			]);

	   			flash()->success('New Record Added', 'New User Hasbeen Added');
	   			return redirect()->back();
	        }

	        flash()->success('OOPS', 'User Found but with Other Zip Code');
	   		return redirect()->back();
   		} catch (\Exception $e) {
   			dd($e);
   		}
   	}

   	/**
   	 * Function to display User details page
   	 * 
   	 * @param  [id] $id must be int
   	 * @return [view] admin.user.details
   	 */
   	public function userDetails(int $id)
   	{
   		try {
   			$data = $this->user->where('id', $id)
                                ->with('profile')
                                ->first();

   			if (empty($data)) {
   				flash()->info('OOPS!', 'Try Again with valide ID');
   				return redirect()->route('admin.user.list');
   			}

   			$zip_code = $this->zip_code->where('code', $data->zip_code)->first();
   			$zip_codes = $this->getZipCodeArray($this->zip_code->latest('created_at')->get(), true);

   			return view('admin.users.details', compact('data', 'zip_codes', 'zip_code'));
   		} catch (\Exception $e) {
   			dd($e);
   		}
   	}

    /**
     * orderLists method for listing the all orders
     * 
     * @return view     admin.orders.lists
     */
    public function orderLists()
    {
        try {
            $orders = $this->order
                                ->where('status', 0)
                                ->with('payment')
                                ->with('user')
                                ->with('fuelDetail')
                                ->latest('created_at')->get();

            return view('admin.orders.incomplete', compact('orders'));
        } catch (\Exception $e) {
            dd($e);
        }
    }

    /**
     * paymentList method for listing all payments
     * 
     * @return view admin.orders.lists
     */
    public function paymentList()
    {
        try {
            $payments = $this->payment
                                ->with('order')
                                ->with('user')
                                ->with('order.fuelDetail')
                                ->latest('created_at')->get();
                               

            return view('admin.orders.lists', compact('payments'));
        } catch (Exception $e) {
            dd($e);
        }
    }

    /**
     * orderDetails method for showing order details 
     * 
     * @param  int      $id Order ID
     * @return view     admin.orders.details
     */
    public function orderDetails(int $id)
    {
        try {
            $payment = $this->payment->where('id', $id)
                                    ->with('order')
                                    ->with('user')
                                    ->with('order.fuelDetail')
                                    ->with('order.tankDetail')
                                    ->with('order.instruction')
                                    ->with('order.profile')
                                    ->with('order.addOn')
                                    ->first();

            return view('admin.orders.details', compact('payment'));
        } catch (\Exception $e) {
            dd($e);
        }
    }

   	/** 
   	 * Function to update the Requested User
   	 * 
   	 * @param  Request
   	 * @return [redirect] route('admin.user.list')
   	 */
   	public function updateUser(Request $request)
   	{
   		try {
   			$data = $this->user->where('id', $request->id)->first();

   			if (empty($data)) {
   				flash()->info('OOPS!', 'Try Again with valide ID');
   				return redirect()->route('admin.user.list');
   			}	

   			$zip_code = $this->zip_code->where('id', $request->zip_code)->first();

   			if (empty($zip_code)) {
   				flash()->info('OOPS', 'Zip Code is not valid');
   				return redirect()->back();
   			}

   			$data->update([
   				'zip_code_id'		=> $zip_code->code,
   				'email' 			=> $request->email,
   				'name'				=> $request->name
   			]);

   			flash()->success('Record Updated', 'user Record Hasbeen updated');
   			return redirect()->route('admin.user.list');
   		} catch (\Exception $e) {
   			dd($e);
   		}
   	}
}
