<?php

namespace App\Http\Controllers;

use Auth;
use App\{ User, ZipCode, FuelRate, Order};
use App\Http\Requests\{ FuelDetailRequest, TankLocationRequest, DeliveryInstructionRequest, UserProfileRequest };
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FuelsController extends Controller
{
    /**
     * @param User
     * @param ZipCode
     * @param FuelRate
     */
    public function __construct(User $user, ZipCode $zip_code, FuelRate $fuel_rate, Order $order)
    {
    	$this->middleware('auth');
    	$this->user = $user;
        $this->order = $order;
    	$this->zip_code = $zip_code;
    	$this->fuel_rate = $fuel_rate;
    }

    /**
     * Function to show Fule Details Page
     * 
     * @return [view] fuel details
     */
    public function showFuelDetailsPage()
    {
    	try {
    		$user = Auth::user();
            $data = $user->fuelDetails()->latest('created_at')->first();
    		$price = $this->getFuelRateForZipCode($user->zip_code);

    		return view('fuel_details', compact('price', 'data'));
    	} catch (\Exception $e) {
    		dd($e);
    	}
    }

    /**
     * Function to store fule details into database
     * 
     * @param  FuelDetailRequest to verify the Incoming Request
     * @return [redirect] @route tank.location.view
     */
    public function storeFuelDetails(Request $request)
    {
    	try {
            if($request->fill_tank!='1'){
                $validator = Validator::make($request->all(), [
                    'gallons'   => 'required_without:amount|nullable|integer',
                    'amount'    => 'required_without:gallons|nullable|integer'
                ]);
                if ($validator->fails()) {
                  return redirect('/fuel-details')->withErrors($validator)->withInput();
                }
            }
    		$user = Auth::user();
            if ($request->gallons != null && $request->amount != null) {
                flash()->info('OOPS', 'You can select only one value i.e. Gallon or Amount');
                return redirect()->back()->withInput($request->all());
            }

            if ($request->gallons != null && $request->gallons < 150) {
                flash()->info('OOPS', 'You can\'t order less then 150 gallons');
                return redirect()->back()->withInput($request->all());
            }

            if ($request->amount != null && $request->amount < ($this->getFuelRateForZipCode($user->zip_code) * 150)) {
                $price = $this->getFuelRateForZipCode($user->zip_code) * 150;
                flash()->info('OOPS', "You can't order less then $ $price");
                return redirect()->back()->withInput($request->all());
            }
            
            $data = $user->fuelDetails()->updateOrCreate([
                'gallons'       => $request->gallons,
                'amount'        => $request->amount,
                'need_express'  => $request->need_express,
                'prime_start'   => $request->prime_start,
                'fill_tank'     => $request->fill_tank
            ]);

            $this->initSessionData($request)->addStepsSessionData($request, 'step_1', $data);

    		return redirect()->route('tank.location.view');
    	} catch (\Exception $e) {
    		dd($e);
    	}
    }

    /**
     * Function to show the tank location page
     * 
     * @return [view] tank_location
     */
    public function showTankLocationPage()
    {
    	try {
            $user = Auth::user();
            $data = $user->tankDetails()->latest('created_at')->first();
            $capacities_arr=$this->getCapacityArray();
            $how_much_arr = $this->getHowmuchArray();
            return view('tank_location', compact('data','capacities_arr','how_much_arr'));
    	} catch (\Exception $e) {
    		dd($e);
    	}
    }

    /**
     * Function to store the tank location details into Database
     * 
     * @param  TankLocationRequest to verify the Incoming Request
     * @return [redirect] route delivery.instruction.view
     */
    public function storeTankLocation(TankLocationRequest $request)
    {
    	try {
    		$user = Auth::user();
            $data = $user->tankDetails()->updateOrCreate([
                'tank_location'     => $request->tank_location,
                'tank_capacity'     => $request->tank_capacity,
                'current_status'    => $request->current_status
            ]);   

            $this->addStepsSessionData($request, 'step_2', $data);

    		return redirect()->route('delivery.instruction.view');
    	} catch (\Exception $e) {
    		dd($e);
    	}
    }

    /**
     * Function to show delivery instruction page
     * 
     * @return [view] delivery_instruction
     */
    public function showDeliveryInstructionPage()
    {
    	try {
            $user = Auth::user();
            $data = $user->instruction()->latest('created_at')->first();

    		return view('delivery_instruction', compact('data'));
    	} catch (\Exception $e) {
    		dd($e);
    	}
    }

    /**
     * Function to store the delivery instruction into database
     * 
     * @param  DeliveryInstructionRequest to verify the Incoming Request
     * @return [redirect] route user.data.view
     */
    public function storeDeliveryInstruction(DeliveryInstructionRequest $request)
    {
    	try {
    		$user = Auth::user();

            $data = $user->instruction()->updateOrCreate([
                'auto_delivery' => $request->save_data,
                'instruction'   => $request->instruction
            ]);   

            $this->addStepsSessionData($request, 'step_3', $data);

    		return redirect()->route('user.data.view');
    	} catch (\Exception $e) {
    		dd($e);
    	}
    }

    /**
     * Function to show user details page
     * 
     * @param  int|null $id 
     * @return [view] user_details
     */
    public function showUserDetailsPage(int $id = null)
    {
    	try {
            $data = [];
            $display_prop = 'block';
    		$titles = $this->getTitles();
            $profiles = Auth::user()->profile()->latest('created_at')->get();

            if (count($profiles)) {
                $display_prop = 'none';
            }

    		return view('user_details', compact('titles', 'profiles', 'display_prop'));
    	} catch (\Exception $e) {
    		dd($e);
    	}
    }

    /**
     * Function to store user details into database
     * 
     * @param  UserProfileRequest to verify the Incoming Request
     * @return [redirect] route services.view
     */
    public function storeUserDetails(UserProfileRequest $request)
    {
    	try {
    		$user = Auth::user();

            $user->update([
                'name'          => $request->title . ' ' . $request->first_name . ' ' . $request->last_name, 
            ]);

    		$data = $user->profile()->create([
                'title'         => $request->title,
                'first_name'    => $request->first_name,
                'last_name'     => $request->last_name,
    			'street'		=> $request->street, 
    			'city' 			=> $request->city,
    			'phone' 		=> $request->phone,
    			'color_home'	=> $request->color_home,
    			'contact_on'	=> $request->contact_on, 
    			'ref_by'		=> $request->ref_by
    		]);

            $this->addStepsSessionData($request, 'step_4', $data);

    		return redirect()->route('services.view');
    	} catch (\Exception $e) {
    		dd($e);
    	}
    }

    /**
     * Function to update user profile details
     * 
     * @param  UserProfileRequest to verify the Incoming Request
     * @return [redirect] route services.view
     */
    public function updateUserDetails(UserProfileRequest $request)
    {
        try {
            $user = Auth::user();

            $profile = $user->profile()->where('id', $request->id)->first();
            
            if (empty($profile)) {
                flash()->info('OOPS', 'Profile not found');
                return redirect()->back();
            }

            $data = $profile->update([
                'title'         => $request->title,
                'first_name'    => $request->first_name,
                'last_name'     => $request->last_name,
                'street'        => $request->street, 
                'city'          => $request->city,
                'phone'         => $request->phone,
                'color_home'    => $request->color_home,
                'contact_on'    => $request->contact_on, 
                'ref_by'        => $request->ref_by,
                'is_existing'   => $request->is_existing
            ]);
            
            $profile = $user->profile()->where('id', $request->id)->first();

            $this->addStepsSessionData($request, 'step_4', $profile);

            return redirect()->route('services.view');
        } catch (\Exception $e) {
            dd($e);
        }
    }

    /**
     * Function to show the view of up sell page
     * 
     * @return [view] services
     */
    public function showServicesPage(Request $request)
    {
    	try {
            $services = $this->getServices();
            $selected = $this->getStepSessionData($request, 'step_5');
            $selected == null ? $selected = [] : null;
    		return view('services', compact('services', 'selected'));
    	} catch (\Exception $e) {
    		dd($e);
    	}
    }

    /**
     * Function To store selected services in session
     * 
     * @param  Request
     * @return [redirect] route('user.checkout.view')
     */
    public function storeServices(Request $request)
    {
    	try {
            $this->addStepsSessionData($request, 'step_5', $request->service);
            $user = Auth::user();
            $data = $this->getAllStepData($request);
            $order = $user->order()->create([
                'fuel_detail_id'    => $data['step_1']['id'],
                'tank_detail_id'    => $data['step_2']['id'],
                'instruction_id'    => $data['step_3']['id'],
                'profile_id'        => $data['step_4']['id'],
                'status'            => '0',
                'amount'            => $this->getTotalAmount($data),
                'fule_rate'         => $this->getFuelRateForZipCode(Auth::user()->zip_code),
            ]);

            $this->addStepsSessionData($request, 'step_6', $order);

            if (isset($request->service)) {
                foreach ($request->service as $service) {
                    $order->addOn()->attach($service);
                }   
            }
            
    		return redirect()->route('user.checkout.view');
    	} catch (\Exception $e) {
    		dd($e);
    	}
    }

    public function storeOrder(Request $request,int $id)
    {
        try {
            $order = $this->order->where('id', $id)->first();

            if (empty($order)) {
                flash()->error('Error', 'no order was found');
                return redirect()->back()->withInput($request->all()); 
            }

            $res = $order->update([
                'status'    => 1
            ]);
            
            if($res) {
                flash()->success('Success', 'Order has been placed');
                return redirect()->back()->withInput($request->all());
            }
            
            flash()->error('Error', 'Order has not placed');
            return redirect()->back()->withInput($request->all());            
        } catch (\Exception $e) {
            dd($e);
        }
    }
}
