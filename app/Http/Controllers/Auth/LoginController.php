<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/fuel-details';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        //dd($request->all());
        if (!$this->checkZipCode($request->zip_code)) {
            flash()->info('OOPS!', 'We Don\'t Ship In This Area.');
            return redirect()->back()->withInput($request->all());;
        }

        $user = $this->checkUserStatus($request->email, $request->zip_code);       

        if (!$user) {
            flash()->error('OOPS!', 'Internal Server Error.');
            return redirect()->back()->withInput($request->all());;
        }

        if ($user == 'blocked') {
            flash()->info('OOPS!', 'Not Authorized To Perform This Action.');
            return redirect()->back()->withInput($request->all());;
        }

        if ($user == 'not found') {
            $user = RegisterController::create($request->all());
        }

        Auth::login($user);
        flash()->success('Welcome User', '');
        return redirect()->intended($this->redirectTo);
    }
}
