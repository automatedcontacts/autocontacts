<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         => 'required',
            'first_name'    => 'required',
            'last_name'     => 'required',
            'street'        => 'required', 
            'city'          => 'required',
            'phone'         => 'required|integer',
            'color_home'    => 'required',
            'contact_on'    => 'required', 
        ];
    }
}
