<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FuelRate extends Model
{
    use SoftDeletes;

    protected $fillable = [
    	'rate_per_gallon', 'zip_code_id', 'is_global'
    ];

    public function zipCode()
    {
    	return $this->belongsTo('App\ZipCode');
    }
}
