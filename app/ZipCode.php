<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ZipCode extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'name', 'code',
    ];

    public function fuelRate()
    {
    	return $this->hasOne('App\FuelRate');
    }
}
