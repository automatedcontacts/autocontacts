<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{

	use SoftDeletes;

    protected $fillable = [
    	'user_id',
		'fuel_detail_id',
		'tank_detail_id',
		'instruction_id',
		'profile_id',
		'fuel_rate_id',
		'amount',
        'status',
        'fule_rate'
    ];


    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function payment()
    {
    	return $this->hasOne('App\Payment');
    }

    public function fuelDetail()
    {
    	return $this->belongsTo('App\FuelDetail');
    }

    public function tankDetail()
    {
    	return $this->belongsTo('App\TankDetail');
    }

    public function instruction()
    {
    	return $this->belongsTo('App\Instruction');
    }

    public function profile()
    {
    	return $this->belongsTo('App\Profile');
    }

    public function fuelRate()
    {
    	return $this->belongsTo('App\FuelRate');
    }

    public function addOn()
    {
    	return $this->belongsToMany('App\AdditionalService');
    }
}
