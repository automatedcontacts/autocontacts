<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HowMuchTank extends Model
{
    protected $table = 'how_much_tanks';
    protected $fillable = ['how_much_tank'];
}
