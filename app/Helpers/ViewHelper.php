<?php
	namespace App\Helpers;
	use App\TankCapacity;

	use Illuminate\Http\Request;

	class ViewHelper
	{
		use Helper;

		function __construct(Request $request)
		{
			$this->request = $request;
		}

		public function getDataForOrderDetails($view)
		{
			try {
				$data = $this->getOrderDetails($this->request);
				$view->with('data', $data);
			} catch (\Exception $e) {
				dd($e);
				return false;
			}
		}
		
	}