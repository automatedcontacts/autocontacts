<?php 
namespace App\Helpers;

use Auth;
use App\{ User, ZipCode, FuelRate, AdditionalService,TankCapacity,HowMuchTank};

trait ControllersHelper {

	use Helper;

	protected function checkZipCode(int $zip_code, $return_value = false)
	{
		try {
			$zip_code_db = ZipCode::where('code', $zip_code)->first();

			if (empty($zip_code_db)) {
				return false;
			}

			if ($return_value) {
				return $zip_code_db;
			}

			return true;
		} catch (\Exception $e) {
			return false;
		}
	}	

	protected function checkUserStatus(string $email, int $zip_code, $is_admin = false)
	{
		try {
			$user = User::where('email', $email)->withTrashed()->first();

			if (empty($user)) {
				return "not found";
			}

			if ($user->deleted_at != null) {
				return 'blocked';
			}

			if ($user->zip_code != $zip_code && !$is_admin) {
				$user->update([
					'zip_code'	=> $zip_code
				]);
			}

			return $user;
		} catch (\Exception $e) {
			return false;
		}
	}

	protected function getGlobalFuelRate()
	{
		try {
			$rate = FuelRate::where('is_global', 1)->first();

			return $rate->rate_per_gallon;
		} catch (\Exception $e) {
			return false;
		}
	}

	protected function getFuelRateForZipCode(int $zip_code)
	{
		try {
			$zip_code_db = $this->checkZipCode($zip_code, true);
			$fuel_db = $zip_code_db->fuelRate()->first();

			if (empty($fuel_db)) {
				return $this->getGlobalFuelRate();
			}
			$global_rate=$this->getGlobalFuelRate();
            $rate = $global_rate + $fuel_db->rate_per_gallon;
			return $rate;
		} catch (\Exception $e) {
			return false;
		}
	}

	protected function getPriceForGallon(int $count, int $zip_code = null)
	{
		try {
			if ($zip_code == null) {
				return $this->getGlobalFuelRate() * $count;
			}

			return $this->getFuelRateForZipCode($zip_code) * $count;
		} catch (\Exception $e) {
			return false;
		}
	}

	protected function getTitles()
	{
		return [
			''		=> 'Select One',
			'Mr.'	=> 'Mr.',
			'Mis.'	=> 'Mis.',
			'Mrs.'  => 'Mrs.',
			'Er.'	=> 'Er.',
			'Dr.'	=> 'Dr.',
			'Prof.'	=> 'Prof.'
		];
	}

	protected function getServices()
	{
		try {
			return AdditionalService::latest('created_at')->get();
		} catch (\Exception $e) {
			return [];
		}
	}

	protected function getZipCodeArray($zip_codes, $exc_glo = false)
	{
		try {
			if ($exc_glo) {
				$result = [];
			} else {
				$result[''] = ['Global'];
			}

			foreach ($zip_codes as $zip_code) {
				$result[$zip_code->id] = $zip_code->name;
			}

			return $result;
		} catch (\Exception $e) {
			dd($e);
		}
	}

	protected function getTotalAmount($data)
	{
		try {
			$rate = 0;

			if ($data['step_1']['gallons'] != null) {
				$rate = $this->getPriceForGallon($data['step_1']['gallons'], Auth::user()->zip_code);
			}

			if ($data['step_1']['fill_tank'] != null) {
				$rate = $this->getPriceForGallon($data['step_2']['tank_capacity'], Auth::user()->zip_code);
			}

			if ($data['step_1']['amount'] != null) {
				$rate = $data['step_1']['amount'];
			}

			if ($data['step_1']['prime_start'] != null) {
				$rate = $rate + $this->getPrimeStartPrice();
			}

			if ($data['step_1']['need_express'] != null) {
				$rate = $rate + $this->getExpressDeliveryPrice();
			}

			if ($data['step_5'] != null) {
				foreach ($data['step_5'] as $addons) {
					$rate = $rate + $this->getAddOnServicePrice($addons)->price;
				}	
			}

			return round($rate, 2);
		} catch (\Exception $e) {
			dd($e);
		}
	}

	public function getCapacityArray()
	{
		try {
			$capacities=TankCapacity::all();
            $testarray = [];
            $testarray['']="Select Capacity";
            foreach ($capacities as  $capacity) {
            	$testarray[$capacity->capacity] = $capacity->capacity;
            }
            return $testarray;
		}catch (\Exception $e) {
			return false;
		}
	}

	public function getHowmuchArray()
	{
		try {
			$how_much_tanks=HowMuchTank::all();
            $testarray = [];
            $testarray['']="Select one";
            foreach ($how_much_tanks as  $how_much_tank) {
            	$testarray[$how_much_tank->how_much_tank] = $how_much_tank->how_much_tank;
            }

            return $testarray;
		} catch (\Exception $e) {
			return false;
		}  
	}	
}