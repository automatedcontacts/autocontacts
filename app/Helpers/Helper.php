<?php 
namespace App\Helpers;

use App\AdditionalService;
use Illuminate\Http\Request;

trait Helper {
	
	protected function initSessionData(Request $request)
	{
		try {
			$request->session()->put('step_1', []);
			$request->session()->put('step_2', []);
			$request->session()->put('step_3', []);
			$request->session()->put('step_4', []);
			$request->session()->put('step_5', []);
			$request->session()->put('step_6', []);
		
			return $this;
		} catch (\Exception $e) {
			dd($e);
		}
	}

	protected function resetSessionData(Request $request)
	{
		try {
			$request->session()->flush();

			return $this;
		} catch (\Exception $e) {
			dd($e);
		}
	}

	protected function addStepsSessionData(Request $request, string $step, $data)
	{
		try {
			$request->session()->forget($step);

			$request->session()->put($step, $data);

			return $this;
		} catch (\Exception $e) {
			dd($e);
		}
	}

	protected function getStepSessionData(Request $request, string $step, $default = null)
	{
		try {
			return $request->session()->get($step, $default);
		} catch (\Exception $e) {
			dd($e);
		}
	}

	protected function getAllStepData(Request $request)
	{
		try {
			$result = [
				'step_1' => $this->getStepSessionData($request, 'step_1'),
				'step_2' => $this->getStepSessionData($request, 'step_2'),
				'step_3' => $this->getStepSessionData($request, 'step_3'),
				'step_4' => $this->getStepSessionData($request, 'step_4'),
				'step_5' => $this->getStepSessionData($request, 'step_5'),
				'step_6' => $this->getStepSessionData($request, 'step_6'),
			];

			return $result;
		} catch (\Exception $e) {
			dd($e);
		}
	}

	protected function getOrderDetails(Request $request)
	{
		try {
			return $this->getAllStepData($request);
		} catch (\Exception $e) {
			dd($e);
		}
	}

	protected function getPrimeStartPrice()
	{
		try {
			return 25;
		} catch (\Exception $e) {
			return 0;
		}
	}

	protected function getExpressDeliveryPrice()
	{
		try {
			return 0;
		} catch (\Exception $e) {
			return 0;
		}
	}

	protected function getAddOnServicePrice($id)
	{
		try {
			return AdditionalService::where('id', $id)->first();
		} catch (\Exception $e) {
			dd($e);
		}
	}
}