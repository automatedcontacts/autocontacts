@extends('layouts.app')

    

@section('content')

    <div class="panel-heading heading"><h4>Fuel Price Per Gallon:</h4> <span class="pull-right">$ {{ $price }}</span></div>
    <div class="panel-body row">
        <div class="col-md-12">
            <h4 class="pull-right number-code">Delivery at: {{ Auth::user()->zip_code }}</h4>
        </div>
        {!! Form::model($data, ['route' => 'fuel.details.save', 'class' => 'form-horizontal']) !!}
            <div class="form-group{{ $errors->has('gallons') ? ' has-error' : '' }}">
                <label for="gallons" class="col-md-4 control-label">Specify Gallons :</label>
                <div class="col-md-10">
                    {!! Form::text('gallons', old('gallons'), ['class' => 'form-control gallon', 'placeholder' => 'Minimum Order: 150 gallons']) !!}

                    @if ($errors->has('gallons'))
                        <span class="help-block">
                            <strong>{{ $errors->first('gallons') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                <label for="amount" class="col-md-4 control-label">Specify Amount ( $ ) :</label>
                <div class="col-md-10">
                    {!! Form::text('amount', old('amount'), ['class' => 'form-control amount', 'placeholder' => "Minimum Order Amount: $" . $price * 150]) !!}
                    @if ($errors->has('amount'))
                        <span class="help-block">
                            <strong>{{ $errors->first('amount') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="need_express" class="col-md-4 control-label"></label>
                <div class="col-md-10">
                    {!! Form::checkbox('need_express', 1) !!}
                    Out of oil - Need Express Delivery ( $ 0 )
                    @if ($errors->has('need_express'))
                        <span class="help-block">
                            <strong>{{ $errors->first('need_express') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="prime_start" class="col-md-4 control-label"></label>
                <div class="col-md-10">
                    {!! Form::checkbox('prime_start', 1) !!}
                    Need Prime Start ( $ 25 )
                    @if ($errors->has('prime_start'))
                        <span class="help-block">
                            <strong>{{ $errors->first('prime_start') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="prime_start" class="col-md-4 control-label"></label>
                <div class="col-md-10" >
                    {!! Form::checkbox('fill_tank', 1, null, ['id' => 'fill_tank']) !!}
                    Fill Tank?
                    @if ($errors->has('fill_tank'))
                        <span class="help-block">
                            <strong>{{ $errors->first('fill_tank') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-10 col-md-offset-2">
                    {!! Form::submit('Next', ['class' => 'btn btn-primary pull-right']) !!}
                </div>
            </div>
        {!! Form::close() !!}
    </div>

@endsection
@section('custom-js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() { 
        $('#fill_tank').change(function() {
            if($(this).is(":checked")) {
               $('.gallon').val(""); 
               $('.amount').val("");
               $('.gallon').attr('readonly', 'true');
               $('.amount').attr('readonly', 'true'); 
            } else { 
                $(".gallon").prop('readonly', false);
                $(".amount").prop('readonly', false);   
            }
        });
    });

    $(document).ready(function() {
        if ($("#fill_tank").prop('checked') == true) {
            $('.gallon').val(""); 
            $('.amount').val("");
            $('.gallon').attr('readonly', 'true');
            $('.amount').attr('readonly', 'true'); 
        } 
    });
</script>

@endsection