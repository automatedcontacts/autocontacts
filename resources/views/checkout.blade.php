@extends('layouts.app')

@section('content')
    <div class="panel-heading heading"><h4>Order Summary</h4></div>
    <div class="panel-body row">
        <div class="col-md-12">
            @if ($is_old)
                <center>
                    <h2>CheckOut With Previous Order</h2>
                </center>
            @endif
            @if (!empty($order))
                <div class="delivery-details">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <b>Billed To:</b>
                            <p>{{ $order->user->name }}</p>
                            <p>{{ $order->user->email }}</p>
                        </div>
                        <div class="col-md-6 col-sm-6"> 
                            <span class="pull-right">
                                <b>Shipped To:</b>
                                <p>{{ $order->profile->title }} {{ $order->profile->first_name }} {{ $order->profile->last_name }}</p>
                                <p>{{ $order->profile->street }}, {{ $order->profile->city }}, {{ $order->user->zip_code }}</p>
                                <p>House Color: {{ $order->profile->color_home }}</p>
                                <p>{{ $order->profile->phone }}</p>
                            </span>   
                        </div>
                    </div>
                </div>
                <br>
                <br>
                <table class="table table-striped delivery-table" width="100%">
                    <thead>
                        <tr>
                            <th>Item</th>
                            <th>Value</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            @if($order->fuelDetail->fill_tank != '1')
                                <td>@if ($order->fuelDetail->gallons) Gallons @else Amount @endif</td>
                                <td>@if ($order->fuelDetail->gallons) {{ $order->fuelDetail->gallons }} @else {{ $order->fuelDetail->amount }} @endif</td>
                            @else    
                                <td>Fill The Tank</td>
                                <td>YES</td>
                            @endif    
                        </tr>
                        <tr>
                            <td>Per Gallon Price</td>
                            <td>$ {{ $order->fule_rate }}</td>
                        </tr>
                        <tr>
                            <td>Need Express Delivery</td>
                            <td>@if ($order->fuelDetail->need_express) YES @else NO @endif</td>
                        </tr>
                        <tr>
                            <td>Need Prime Start</td>
                            <td>@if ($order->fuelDetail->prime_start) YES @else No @endif</td>
                        </tr>
                        @if (!empty($order->addOn))
                            @foreach ($order->addOn as $addon)
                                <tr>
                                    <td>{{ $addon->value }}</td>
                                    <td>$ {{ $addon->price }}</td>    
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>Total:</td>
                            <td>$ {{ $order->amount }} @if($order->fuelDetail->fill_tank == '1') <small>(Based on your tank capacity)</small>@endif</td>
                        </tr>
                    </tfoot>
                </table>
                @if($order->amount > 0)
                    <button class="btn btn-primary pull-right" disabled=true id="customButton">Pay Now</button>
                @else
                    <a href='{{URL::to("place_order/$order->id")}}' class="btn btn-primary pull-right">Place Order</a>
                @endif   
                <input id="terms" type="checkbox" onchange="document.getElementById('customButton').disabled = !this.checked;" name="termsAndConditions"> 
                <a href="javascript:void(0)" class="trm-btn" data-toggle="modal" data-target="#myModal">I Agree to the Terms and Conditions </a>
                </div>
                
            @endif
            @if (empty($order))
                <center>
                    <h2>Nothing Found for Order, Or Missing Data. ReCheck Your Values Or Place New Order</h2>
                    <br>
                    <div class="form-group">
                        <label for="need_express" class="col-md-4 control-label"></label>
                        <div class="col-md-10" id="terms">
                            {!! Form::checkbox('need_express', 1 ,true) !!}
                            I agree to the Terms and Conditions
                            @if ($errors->has('need_express'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('need_express') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <br>

                    <button class="btn btn-primary" id="sendToStart" >Start Here</button>
                </center>
            @endif
        </div>
        <!-- popup box start  -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">X</button>
                        <h4 class="modal-title">The Terms and Conditions for Meadows App:</h4>
                    </div>
                    <div class="modal-body">
                        <p>NOTICE:: ORDERS WILL BE DELIVERED WITHIN 2-3 BUSINESS DAYS of your order. If you are in need an EMERGENCY OIL DELIVERY OR IT IS after business hours or weekend or holiday, you MUST CALL THE OFFICE to page the person on call, as online orders are NOT processed after normal business hours.

                        UNDELIVERED GALLONS: A credit will be applied to your account for undelivered gallons, billed at the current price of your next delivery order. We can not return, or remove heating oil once it has been delivered, all orders placed will be delivered, any undeliverable gallons will be applied to your account as a dollar amount credit toward your next delivery order. Credit Card will only be refunded if requested via email. YOU WILL RECEIVE an Email CONFIRMATION of your order: Please check your email and your spam folder for payment confirmation, if you do not receive confirmation via email your order did not process and it will be necessary to contact the office right away as your order will not be processed. You can reply to the confirmation email with specific delivery instructions, OR IF YOU WOULD LIKE YOUR TANK FILLED UP or any other concerns. Please include your name & number when replying.</p>
                        <p>CONDITION & LOCATION OF FUEL STORAGE TANKS & PIPING: I certify that my fuel storage tank and all connected piping are in good operating condition, free from leaks, and installed according to local building code. I understand that you are prohibited from making deliveries in cases where my equipment is not in proper operating order, such as inoperable vent alarm (whistle), PVC piping, or other unsafe or environmentally risky circumstance. The oil storage tank and all associated piping belong to you or the property owner, and Meadows Petroleum Products Inc. neither assumes or accepts any responsibility for this equipment or its maintenance and any remediation requirements resulting from spills, leaks or overfills due to malfunction of this equipment, and are the sole responsibility of the tank owner. Meadows Petroleum Products Inc. will accept no responsibility and will not be liable for damages that may result from a heating failure, or for any spills beyond the fill connection regardless of the cause. I also understand that if a delivery is attempted at my property but my fuel storage system is found to be in an unsafe condition, OR my tank will not take at least the minimum delivery requirement of 125 Gallons I may incur a delivery fee. If you have more than one oil storage tank located on your property, it is your responsibility to clearly label each tank to prevent accidental delivery to the wrong tank. Meadows Petroleum Products Inc. will not responsible for oil delivered to the incorrect tank. We are not responsible for deliveries to wrong tanks when tanks are not properly labeled and we have not been notified in writing that there are multiple oil tanks on the property. 

                        Online Orders: may be delivered any time after the order is placed, but will be within the 3 Business Days that follow (Weather and driveway conditions permitting). Orders placed after normal office hours are considered placed the following business day. ie: Order placed on a Monday at 8pm will be considered to be an order placed as a Tuesday order and delivery will be made by Friday.                                         

                        Cancelling Orders: I understand that once my order is confirmed, and due to the automated nature of our order and delivery system, my order cannot be cancelled.  Any cancelled or refused deliveries will be posted as "0" gallons delivered and will incur a minimum fee of $50.

                        PRIME & RESTART: If you have run out of oil and your system requires a prime and re-start you must be at the house for driver at the time of delivery. If other problems are present and priming is not working you will still be charged for the service to attempt to re-start the system. The delivery drivers are not trained service technicians.

                        EXPRESS DELIVERY: (When Available) Is for same day priority delivery during our normal business hours only. If you need an emergency delivery after business hours or weekend or holiday, you MUST CALL THE OFFICE to page the person on call, as online orders are NOT processed after normal business hours.
                    </p>
                </div>
            </div>
        </div>
        <!-- popup box close -->
    </div>
@endsection

@section ('custom-js')
    @if(!empty($order))
        <script src="https://checkout.stripe.com/checkout.js"></script>
       <!--  <script type="text/javascript">
            $( document ).ready(function() {
                var check=$('.term').find('input[type=checkbox]:checked');
                alert(check);
            });
        </script> -->
        <script>
            let amount = {{ $order->amount * 100 }}
            let order_id = {{ $order->id }}
            var handler = StripeCheckout.configure({
                key: "{{ env('STRIPE_SECRET_KEY') }}",
                image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
                locale: 'auto',
                token: function(token) {
                    var APP_URL = {!! json_encode(url('/')) !!};
                    location.href = APP_URL+`/process-payment/${ token.id }/${ order_id }/`
                    // location.href = `http://localhost:8888/meadows/public/process-payment/${ token.id }/${ order_id }`
                }
            });

            document.getElementById('customButton').addEventListener('click', function(e) {
                // Open Checkout with further options:
                handler.open({
                    name: 'Meadows',
                    description: 'Online Oil Store',
                    amount: amount
                });
                e.preventDefault();
            });

            // Close Checkout on page navigation:
            window.addEventListener('popstate', function() {
                handler.close();
            });
        </script>
    @endif
    @if(empty($order))
        <script type="text/javascript">
            document.getElementById('sendToStart').addEventListener('click', function(e) {
                // var check =document.getElementById('terms input[type="checkbox"]').checked;
                // alert(check);
                location.href = `${ location.origin }/fuel-details`
            });
        </script>
    @endif
@endsection