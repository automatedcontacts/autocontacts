@extends('layouts.auth_layout')

@section('content')


<div class="hoz-center">

<div class="container center">
    <div class="row">
        <div class="col-md-7 login-section">
            <div class="login-box">
                <div class="login-heading">Login In</div>
                <div class="panel-body">
                    {!! Form::open(['route' => 'login', 'class' => 'form-horizontal']) !!}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">E-Mail Address</label>
                            <div>
                                {!! Form::email('email', old('email'), ['class' => 'form-control']) !!}
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('zip_code') ? ' has-error' : '' }}">
                            <label for="zip_code" class="control-label">Zip Code</label>
                            <div>
                                {!! Form::text('zip_code', '', ['class' => 'form-control']) !!}
                                @if ($errors->has('zip_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('zip_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div>
                                {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
