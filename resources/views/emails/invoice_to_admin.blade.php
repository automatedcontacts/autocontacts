@extends('emails.layouts.app')

@section('content')
    <table align="center" cellpadding="0" cellspacing="0" class="container-for-gmail-android" width="100%">
        <tr>
            <td align="left" valign="top" width="100%" style="background:repeat-x url(http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg) #ffffff;">
                <center>
                    <img src="http://s3.amazonaws.com/swu-filepicker/SBb2fQPrQ5ezxmqUTgCr_transparent.png" class="force-width-gmail">
                    <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#ffffff" background="http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg"
                        style="background-color:transparent">
                        <tr>
                            <td width="100%" height="80" valign="top" style="text-align: center; vertical-align:middle;">
                                <!--[if gte mso 9]>
                                <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="mso-width-percent:1000;height:80px; v-text-anchor:middle;">
                                    <v:fill type="tile" src="http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg" color="#ffffff" />
                                    <v:textbox inset="0,0,0,0">
                                        <![endif]-->
                                        <center>
                                            <table cellpadding="0" cellspacing="0" width="600" class="w320">
                                                <tr>
                                                    <td colspan="2" class="pull-left mobile-header-padding-left" style="vertical-align: middle;">
                                                        <a href="{{ url('/') }}">
                                                            <img width="137" height="47" src="http://app.meadowspetroleum.com/img/meadowspetroleumlogo.png" alt="logo">
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </center>
                                        <!--[if gte mso 9]>
                                    </v:textbox>
                                </v:rect>
                                <![endif]-->
                            </td>
                        </tr>
                    </table>
                </center>
            </td>
        </tr>
        <tr>
            <td align="center" valign="top" width="100%" style="background-color: #f7f7f7;" class="content-padding">
                <center>
                    <table cellspacing="0" cellpadding="0" width="600" class="w320">
                        <tr>
                            <td class="header-lg">
                                Hi We have new order!
                            </td>
                        </tr>
                        <tr>
                            <td class="free-text">
                                you can check order full summary in admin section or by clicking on button below.
                            </td>
                        </tr>
                        <tr>
                            <td class="button">
                                <div>
                                    <!--[if mso]>
                                    <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width:155px;" arcsize="15%" strokecolor="#ffffff" fillcolor="#ff6f6f">
                                        <w:anchorlock/>
                                        <center style="color:#ffffff;font-family:Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;">My Account</center>
                                    </v:roundrect>
                                    <![endif]-->
                                    <a href="{{ route('admin.order.details', $payment->id) }}" style="background-color: #719430;border-radius:5px;color:#ffffff;display:inline-block;font-family:'Cabin', Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;line-height:45px;text-align:center;text-decoration:none;width:155px;-webkit-text-size-adjust:none;mso-hide:all;">Admin</a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="w320">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td class="mini-container-left">
                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td class="mini-block-padding">
                                                        <table cellspacing="0" cellpadding="0" width="100%" style="border-collapse:separate !important;">
                                                            <tr>
                                                                <td class="mini-block">
                                                                    <span class="header-sm">Billed To</span>
                                                                    <br /> {{ $user->name }}
                                                                    <br /> {{ $user->email }}
                                                                    <br /> {{ $user->zip_code }}
                                                                    <br /> payment id: {{ $payment->id }}
                                                                    <br />Card No. : xxxx xxxx xxxx {{$card_number}}
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="mini-container-right">
                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td class="mini-block-padding">
                                                        <table cellspacing="0" cellpadding="0" width="100%" style="border-collapse:separate !important;">
                                                            <tr>
                                                                <td class="mini-block">
                                                                    <span class="header-sm">Shipped To</span>
                                                                    <br /> {{ $profile->title }} {{ $profile->first_name }} {{ $profile->last_name }}
                                                                    <br /> {{ $profile->street }}, {{ $profile->city }}, {{ $user->zip_code }}
                                                                    <br /> {{ $profile->phone }}
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </center>
            </td>
        </tr>
        <tr>
            <td align="center" valign="top" width="100%" style="background-color: #ffffff;  border-top: 1px solid #e5e5e5; border-bottom: 1px solid #e5e5e5;">
                <center>
                    <table cellpadding="0" cellspacing="0" width="600" class="w320">
                        <tr>
                            <td class="item-table">
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td class="header-md" colspan="2">
                                            Item Summary # {{ $order->id }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="60px" style="line-height:60px;">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="title-dark" width="450">
                                            Item
                                        </td>
                                        <td class="title-dark" width="110">
                                            Value
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="item-col item">
                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td class="product">
                                                        <span style="color: #4d4d4d; font-weight:bold;">
                                                            @if ($fuel_detail->gallons) 
                                                                Gallons 
                                                            @elseif ($fuel_detail->amount) 
                                                                Amount
                                                            @else
                                                                Fill Tank 
                                                            @endif
                                                        </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="item-col">
                                            @if ($fuel_detail->gallons) 
                                                {{ $fuel_detail->gallons }} 
                                            @elseif ($fuel_detail->amount) 
                                                $ {{ $fuel_detail->amount }}
                                            @else
                                                Yes 
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="item-col item">
                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td class="product">
                                                        <span style="color: #4d4d4d; font-weight: bold;">Per Gallon Price</span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="item-col price">
                                            $ {{ $order->fule_rate }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="item-col item">
                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td class="product">
                                                        <span style="color: #4d4d4d; font-weight: bold;">Need Express Delivery</span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="item-col price">
                                        @if ($fuel_detail->need_express) YES @else NO @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="item-col item">
                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td class="product">
                                                        <span style="color: #4d4d4d; font-weight: bold;">Need Prime Start</span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="item-col price">
                                            @if ($fuel_detail->prime_start) YES @else No @endif
                                        </td>
                                    </tr>
                                    @if (!empty($add_ons))
                                        @foreach ($add_ons as $addon)
                                            <tr>
                                                <td class="item-col item">
                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td class="product">
                                                                <span style="color: #4d4d4d; font-weight: bold;">
                                                                    {{ $addon->value }}
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="item-col price">
                                                    $ {{ $addon->price }}
                                                </td>
                                            </tr> 
                                        @endforeach
                                    @endif
                                    <tr>
                                        <td class="item-col item mobile-row-padding"></td>
                                        <td class="item-col quantity"></td>
                                        <td class="item-col price"></td>
                                    </tr>
                                    <tr>
                                        <td class="item-col quantity" style="text-align:right; padding-right: 10px; border-top: 1px solid #cccccc;">
                                            <span class="total-space" style="font-weight: bold; color: #4d4d4d">Total</span>
                                        </td>
                                        <td class="item-col price" style="text-align: left; border-top: 1px solid #cccccc;">
                                            <span class="total-space" style="font-weight:bold; color: #4d4d4d">$ {{ $order->amount }}</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </center>
            </td>
        </tr>
        <tr>
            <td align="center" valign="top" width="100%" style="background-color: #f7f7f7; height: 100px;">
                <center>
                    <table cellspacing="0" cellpadding="0" width="600" class="w320">
                        <tr>
                            <td style="padding: 25px 0 25px">
                                <strong> Meadows Petroleum Products Inc.</strong>
                                <br /> 21 Brown Stone Road
                                <br /> Mail To: PO Box 187
                                <br />Ottsville PA 18942
                                <br />Main Office. 610.847.4328
                                <br />Fax. 610.847.5257
                                <br />
                                <br />
                                <br />
                                <br />
                                </td>
                            </td>
                        </tr>
                    </table>
                </center>
            </td>
        </tr>
    </table>
@endsection