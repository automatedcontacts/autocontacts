@extends('layouts.app')

@section('content')
    <div class="panel-heading heading"><h4>Delivery Instruction</h4></div>
    <div class="panel-body row">
        <div class="col-md-10 col-md-offset-1">
            <h5>Please Provide us with information we need to ensure that we can access your fuel tank or any details you feel are important.
                <br>
                <b>To ensure delivery indicate cross street to your address or other hints that will help us find your home.</b>
            </h5>
        </div>
        {!! Form::model($data, ['route' => 'delivery.instruction.save', 'class' => 'form-horizontal']) !!}
            <div class="form-group">
                <label for="save_data" class="col-md-4 control-label"></label>
                <div class="col-md-10 col-md-offset-1">
                    {!! Form::checkbox('save_data', 1, false) !!}
                    Interested in Automatic Delivery
                    @if ($errors->has('save_data'))
                        <span class="help-block">
                            <strong>{{ $errors->first('save_data') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('instruction') ? ' has-error' : '' }}">
                <div class="col-md-10 col-md-offset-1">
                    {!! Form::textarea('instruction', old('instruction'), ['class' => 'form-control']) !!}
                    @if ($errors->has('instruction'))
                        <span class="help-block">
                            <strong>{{ $errors->first('instruction') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-10 col-md-offset-1">
                    <a href="{{ route('tank.location.view') }}" class="btn btn-primary pull-left">Previous</a>
                    {!! Form::submit('Next', ['class' => 'btn btn-primary pull-right']) !!}
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection