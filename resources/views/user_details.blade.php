@extends('layouts.app')

@section('content')
    <div class="panel-heading heading"><h4>Shipping Information</h4></div>
    <div class="panel-body row">
        @if (count($profiles))
            <div class="col-md-12" id="profiles-view">
                @foreach ($profiles as $profile)
                    <a href="javascript:void(0)" data-id="{{ $profile->id }}" class="profiles-view-click">
                        <div class="col-md-12"> 
                            <h5>{{ $profile->title }} {{ $profile->first_name }} {{ $profile->last_name }}<span style="float:right; color: #719430">Edit</span></h5>
                            <hr>
                            <p>{{ $profile->street }}, {{ $profile->city }}, {{ Auth::user()->zip_code }} </p>
                            <p>{{ $profile->color_home }}</p>
                            <p>@if ( $profile->contact_on == 'phone' ) {{ $profile->phone }} @else {{ Auth::user()->email }} @endif</p>
                        </div>
                    </a>
                @endforeach
                <div class="col-md-12"> 
                    <center><div class="add-section"><a href="javascript:void(0)" onclick="showAddProfileSection()">+ Add New Address</a></div></center>
                    <hr>
                </div>
                <div class="col-md-12">
                    <a href="{{ route('delivery.instruction.view') }}" class="btn btn-primary pull-left">Previous</a>
                    <a href="javascript:void(0)" onclick="showMessage()" class="btn btn-primary pull-right">Next</a>
                </div>
            </div>
        @endif
        <div class="col-md-12" id="add-profiles" style="display: {{ $display_prop }}">
            {!! Form::open(['route' => 'user.data.save', 'class' => 'form-horizontal']) !!}
                @include ('includes._user_details')
            {!! Form::close() !!}
        </div>

        @foreach($profiles as $data)
            <div class="col-md-12" id="edit-profiles-{{ $data->id }}" style="display: none;">
                {!! Form::model($data, ['route' => 'user.data.update', 'class' => 'form-horizontal']) !!}
                    @include ('includes._user_details', ['id' => $data->id])
                {!! Form::close() !!}
            </div>
        @endforeach
    </div>  
@endsection

@section('custom-js')
<script type="text/javascript">
    function showAddProfileSection() {
        try {
            let profile_view = document.getElementById("profiles-view")
            let add_profile = document.getElementById("add-profiles")
            let previous_button = document.getElementById("handel-previous")

            profile_view.style.display = 'none'
            add_profile.style.display = 'block'

            previous_button.removeAttribute('href')
            previous_button.setAttribute('href', "javascript:void(0)")
            previous_button.setAttribute('onclick', 'showProfileView()')
        } catch (e) {
            console.log(e)
        }
    }

    function showProfileView(id = null) {
        try {
            let profile_view = document.getElementById("profiles-view")
            let add_profile = document.getElementById("add-profiles")

            if (id !== null) {
                let edit_view = document.getElementById(`edit-profiles-${id}`)
                edit_view.style.display = 'none'
            }

            profile_view.style.display = 'block'
            add_profile.style.display = 'none'
        } catch (e) {
            console.log(e)
        }
    }

    function showMessage() {
        swal({
            position: 'bottom-left',
            width: 350,
            type: 'info',
            title: 'Address Not Selected',
            text: 'Select a address or add one',
            showConfirmButton: false,
            timer: 5000
        })
    }

    $('.profiles-view-click').click(function() {
        try {
            let $this = $(this);    
            let id = $this.attr('data-id');

            showEditSection(id)
        } catch (e) {
            console.log(e)
        }
    });

    function showEditSection(id) {
        try {
            let profile_view = document.getElementById("profiles-view")
            let add_profile = document.getElementById("add-profiles")
            let edit_view = document.getElementById(`edit-profiles-${id}`)
            let previous_button = document.getElementById(`handel-previous-${id}`)

            profile_view.style.display = 'none'
            add_profile.style.display = 'none'
            edit_view.style.display = 'block'
                        
            previous_button.removeAttribute('href')
            previous_button.setAttribute('href', "javascript:void(0)")
            previous_button.setAttribute('onclick', `showProfileView(${id})`)
        } catch (e) {
            console.log(e)
        }
    }
</script>
@endsection