<div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Place <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
			{!! Form::select('zip_code_id', $zip_codes, old('zip_code_id'), [
				'class' 						=> 'form-control col-md-7 col-xs-12',
				'required'						=> 'required'
			]) !!}
			@if ($errors->has('zip_code_id'))
            <span class="help-block">
                <strong>{{ $errors->first('zip_code_id') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Rate Per Gallon <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
			{!! Form::text('rate_per_gallon', (old('rate_per_gallon')), [
				'class' 						=> 'form-control col-md-7 col-xs-12',
				'required'						=> 'required'
			]) !!}
			@if ($errors->has('rate_per_gallon'))
            <span class="help-block">
                <strong>{{ $errors->first('rate_per_gallon') }}</strong>
            </span>
        @endif
    </div>
</div>