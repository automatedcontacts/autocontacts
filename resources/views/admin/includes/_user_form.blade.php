<div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::text('name', old('name'), [
                'class'                         => 'form-control col-md-7 col-xs-12',
                'required'                      => 'required'
            ]) !!}
            @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Place <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
			{!! Form::select('zip_code', $zip_codes, $selected_zip_code, [
				'class' 						=> 'form-control col-md-7 col-xs-12',
				'required'						=> 'required'
			]) !!}
			@if ($errors->has('zip_code'))
            <span class="help-block">
                <strong>{{ $errors->first('zip_code') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Email <span class="required">*</span>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
			{!! Form::email('email', (old('email')), [
				'class' 						=> 'form-control col-md-7 col-xs-12',
				'required'						=> 'required',
                'readOnly'                      => $emailProp
			]) !!}
			@if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
</div>