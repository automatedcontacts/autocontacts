@extends ('admin.layouts.app')

@section ('content')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Orders Detail</h3>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              	<div class="col-md-12 col-sm-12 col-xs-12">
                	<div class="x_panel">
                  		<h2>Orders Detail <span class="pull-right"># {{ $payment->order_id }}</span></h2>
                  		<div class="x_content">
                  			<div class="col-md-4">
		                		<b><p>Billed To:</p></b>
		                		<p><a href="{{ route('admin.user.edit', $payment->user->id) }}">{{ $payment->user->name }}</a></p>
		                		<p>{{ $payment->user->email }}</p>
		                		<p>Zip Code: {{ $payment->user->zip_code }}</p>
		                		<br>
		                		<b>Paymant:</b>
		                		<p>Payment ID: {{ $payment->id }}</p>
		                		<p>Customer ID Stripe: {{ $payment->customer_id_stripe }}</p>
		                		<p>Stripe ID: {{ $payment->id_stripe }}</p>
		                		<p>Status: {{ $payment->status }}</p>
		                	</div>
		                	<div class="col-sm-8">
		                		<span class="pull-right">
		                			<b><p>Shipped To:</p></b>
			                		<p>{{ $payment->order->profile->title }} {{ $payment->order->profile->first_name }} {{ $payment->order->profile->last_name }}</p>
			                		<p>{{ $payment->order->profile->street }}, {{ $payment->order->profile->city }}</p>
			                		<p>House Color: {{ $payment->order->profile->color_home }}</p>
			                		<p>{{ $payment->order->profile->phone }}</p>
			                		<br>
			                		<p>{{ $payment->order->created_at->toCookieString() }}</p>	
		                		</span>
		                	</div>
		                	<div class="col-md-12">
		                		<hr>
		                		<h3>Order summary</h3>
		                		<table class="table table-striped table-bordered" cellspacing="0" width="100%">
		                			<thead>
									  	<tr>
									    	<th>Item</th>
									    	<th>Value</th>
									  	</tr>
									</thead>
									<tbody>
										<tr>
									    	<th>
									    		@if ($payment->order->fuelDetail->gallons) 
									    			Gallons
									    		@elseif ($payment->order->fuelDetail->amount)
									    			Amount
									    		@else 
									    			Fill Tank 
									    		@endif
									    	</th>
									    	<th>
									    		@if ($payment->order->fuelDetail->gallons) 
									    			{{ $payment->order->fuelDetail->gallons }} 
									    		@elseif ($payment->order->fuelDetail->amount)
									    			$ {{ $payment->order->fuelDetail->amount }} 
									    		@else 
									    			YES
									    		@endif
									    	</th>
									  	</tr>
									  	<tr>
									  		<th>Per Gallon Price</th>
									  		<th>$ {{ $payment->order->fule_rate }}</th>
									  	</tr>
									  	<tr>
									  		<th>Is existing customer</th>
									  		<th> @if ( $payment->order->profile->is_existing ) Yes @else No @endif</th>
									  	</tr>
									  	<tr>
									  		<th>Need Express Delivery</th>
									  		<th>@if ($payment->order->fuelDetail->need_express) YES @else NO @endif</th>
									  	</tr>
									  	<tr>
									  		<th>Need Prime Start</th>
									  		<th>@if ($payment->order->fuelDetail->prime_start) YES @else No @endif</th>
									  	</tr>
									  	<tr>
									  		<th>Tank Location</th>
									  		<th>{{ $payment->order->tankDetail->tank_location }}</th>
									  	</tr>
									  	<tr>
									  		<th>Tank Capacity</th>
									  		<th>{{ $payment->order->tankDetail->tank_capacity }} gal</th>
									  	</tr>
									  	<tr>
									  		<th>Current Status</th>
									  		<th>{{ $payment->order->tankDetail->current_status }}</th>
									  	</tr>
									  	<tr>
									  		<th>Instruction</th>
									  		<th>{{ $payment->order->instruction->instruction }}</th>
									  	</tr>
									  	<tr>
									  		<th>Auto Delivery</th>
									  		<th>@if ($payment->order->instruction->auto_delivery) YES @else NO @endif</th>
									  	</tr>
									  	@if (!empty($payment->order->addOn))
									  		@foreach ($payment->order->addOn as $addon)
									  			<tr>
									  				<th>Add On</th>
									  				<th>{{ $addon->value }}</th>
									  			</tr>
									  		@endforeach
									  	@endif
									  	<tr>
									  		<th>Total</th>
									  		<th>$ {{ $payment->order->amount }}</th>
									  	</tr>
									</tbody>
								</table>
		                	</div>
                  		</div>
                  	</div>
                </div>
            </div>
        </div>
    </div>    
@endsection

@section ('custom-js')
	<script src="{{ asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('vendors/validator/validator.js') }}"></script>
@endsection