@extends ('admin.layouts.app')

@section ('content')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Orders List</h3>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              	<div class="col-md-12 col-sm-12 col-xs-12">
                	<div class="x_panel">
                  		<div class="x_title">
                  			<h2>Orders List</h2>
                  			<div class="clearfix"></div>
                  		</div>
                  		<div class="x_content">
			                <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			                    <thead>
			                        <tr>
			                        	<th>Order Id</th>
			                        	<th>Client Name</th>
			                        	<th>Existing Client</th>
			                        	<th>Fuel Gallons</th>
			                        	<th>Fuel Amount</th>
			                        	<th>Total Amount</th>
			                        	<th>Fill Tank</th>
			                        	<th>When</th>
			                        	<th>Status</th>
			                        	<th>Action</th>
			                        </tr>
			                    </thead>
			                    <tbody>
			                    	@foreach ($payments as $payment)
				                        <tr>
					                        <td>{{ $payment->id }}</td>
					                       	<td>{{ $payment->user->name }}</td>
					                       	<td>@if ($payment->user->profile[0]->is_existing== 1) Yes @else No @endif</td>
					                       	<td>{{ $payment->order->fuelDetail->gallons }}</td>
					                       	<td>{{ $payment->order->fuelDetail->amount }}</td>
					                       	<td>{{ $payment->order->amount }}</td>
					                       	<td>@if ($payment->order->fuelDetail->fill_tank == 1) Yes @else No @endif</td>
					                       	<th>{{ $payment->created_at->diffForHumans() }}</th>
					                       	<td>@if ($payment->order->status == 1) Payment Done @else Payment Due @endif</td>
					                        <td>
					                        	<a href="@if (!empty($payment->order->status)) {{ route('admin.order.details', $payment->id) }} @else javascript:void(0) @endif">
					                        		<i class="fa fa-eye" aria-hidden="true"></i>
					                        	</a>
					                        </td>
				                        </tr>
				                    @endforeach
			                    </tbody>
			                </table>
			            </div>
                  	</div>
                </div>
            </div>
        </div>
    </div>    
@endsection

@section ('custom-js')
	<script src="{{ asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('vendors/validator/validator.js') }}"></script>
@endsection