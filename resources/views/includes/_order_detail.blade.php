<div class="col-md-3 sidebar hidden-sm hidden-xs">
    <div class="panel panel-default">
    	<div class="panel-heading heading hidden-xs hidden-sm"><h4>Side Bar</h4></div>
        <div class="panel-heading heading hidden-md hidden-lg"><h4>{{ Auth::user()->name }}</h4></div>
    	<div class="panel-body">
            <ul>
                <li>
                    <a {{ Request::is('fuel-details') ? 'class=active' : null }} href="{{ route('fuel.details.show') }}">Fuel Details</a>
                </li>
                <li>
                    <a {{ Request::is('tank-location') ? 'class=active' : null }} @if($data['step_1'] != null) href="{{ route('tank.location.view') }}" @else href="javascript:void(0)" @endif>Tank Fill Location</a>
                </li> 
                <li>
                    <a {{ Request::is('delivery-instruction') ? 'class=active' : null }} @if($data['step_2'] != null) href="{{ route('delivery.instruction.view') }}" @else href="javascript:void(0)" @endif>Delivery Instruction</a>
                </li>
                <li>
                    <a {{ Request::is('user-details') ? 'class=active' : null }} @if($data['step_3'] != null) href="{{ route('user.data.view') }}" @else href="javascript:void(0)" @endif>User Data</a>
                </li>
                <li>
                    <a {{ Request::is('services') ? 'class=active' : null }} @if($data['step_4'] != null) href="{{ route('services.view') }}" @else href="javascript:void(0)" @endif>Add Ons</a>
                </li>
                <li>
                    <a {{ Request::is('checkout') ? 'class=active' : null }} href="{{ route('user.checkout.view') }}">Checkout</a>
                </li>
            </ul>
    	</div>
    </div>
</div>

<div class="mobile-nav hidden-md hidden-lg">
    <div class="toogle-menu hidden-md hidden-lg">
              <span></span>
              <span></span>
              <span></span> 
           </div>
    <div class="panel-heading heading"><h4>{{ Auth::user()->name }}</h4></div>
    <div class="panel-body">
        <ul>
            @if(Auth::user()->is_admin)
                <li>
                    <a href="{{ route('admin.home') }}" target="_blank">DashBoard</a>
                </li>
            @endif
            <li>
                <a {{ Request::is('fuel-details') ? 'class=active' : null }} href="{{ route('fuel.details.show') }}">Fuel Details</a>
            </li>
            <li>
                <a {{ Request::is('tank-location') ? 'class=active' : null }} @if($data['step_1'] != null) href="{{ route('tank.location.view') }}" @else href="javascript:void(0)" @endif>Tank Location</a>
            </li> 
            <li>
                <a {{ Request::is('delivery-instruction') ? 'class=active' : null }} @if($data['step_2'] != null) href="{{ route('delivery.instruction.view') }}" @else href="javascript:void(0)" @endif>Delivery Instruction</a>
            </li>
            <li>
                <a {{ Request::is('user-details') ? 'class=active' : null }} @if($data['step_3'] != null) href="{{ route('user.data.view') }}" @else href="javascript:void(0)" @endif>User Data</a>
            </li>
            <li>
                <a {{ Request::is('services') ? 'class=active' : null }} @if($data['step_4'] != null) href="{{ route('services.view') }}" @else href="javascript:void(0)" @endif>Add Ons</a>
            </li>
            <li>
                <a {{ Request::is('checkout') ? 'class=active' : null }} href="{{ route('user.checkout.view') }}">Checkout</a>
            </li>
            <li>
                <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                    Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
    </div>
</div>
