<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    <label for="title" class="col-md-10 control-label">Title :</label>
    <div class="col-md-12">
        {!! Form::select('title', $titles, null, ['class' => 'form-control']) !!}

        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
    <label for="first_name" class="col-md-10 control-label">First Name :</label>
    <div class="col-md-12">
        {!! Form::text('first_name', old('first_name'), ['class' => 'form-control']) !!}
        @if ($errors->has('first_name'))
            <span class="help-block">
                <strong>{{ $errors->first('first_name') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
    <label for="last_name" class="col-md-10 control-label">Last Name :</label>
    <div class="col-md-12">
        {!! Form::text('last_name', old('last_name'), ['class' => 'form-control']) !!}
        @if ($errors->has('last_name'))
            <span class="help-block">
                <strong>{{ $errors->first('last_name') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group{{ $errors->has('street') ? ' has-error' : '' }}">
    <label for="street" class="col-md-10 control-label">Street# :</label>
    <div class="col-md-12">
        {!! Form::text('street', old('street'), ['class' => 'form-control']) !!}
        @if ($errors->has('street'))
            <span class="help-block">
                <strong>{{ $errors->first('street') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
    <label for="city" class="col-md-10 control-label">City :</label>
    <div class="col-md-12">
        {!! Form::text('city', old('city'), ['class' => 'form-control']) !!}
        @if ($errors->has('city'))
            <span class="help-block">
                <strong>{{ $errors->first('city') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group{{ $errors->has('zip_code') ? ' has-error' : '' }}">
    <label for="zip_code" class="col-md-10 control-label">Zip Code :</label>
    <div class="col-md-12">
        {!! Form::text('zip_code', Auth::user()->zip_code, ['class' => 'form-control', 'readOnly' => true]) !!}
        @if ($errors->has('zip_code'))
            <span class="help-block">
                <strong>{{ $errors->first('zip_code') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
    <label for="phone" class="col-md-10 control-label">Phone :</label>
    <div class="col-md-12">
        {!! Form::text('phone', old('phone'), ['class' => 'form-control']) !!}
        @if ($errors->has('phone'))
            <span class="help-block">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label for="email" class="col-md-10 control-label">Email Address :</label>
    <div class="col-md-12">
        {!! Form::text('email', Auth::user()->email, ['class' => 'form-control', 'readOnly' => true]) !!}
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group{{ $errors->has('color_home') ? ' has-error' : '' }}">
    <label for="color_home" class="col-md-10 control-label">Color of Home :</label>
    <div class="col-md-12">
        {!! Form::text('color_home', old('color_home'), ['class' => 'form-control']) !!}
        @if ($errors->has('color_home'))
            <span class="help-block">
                <strong>{{ $errors->first('color_home') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group">
    <label for="contact_on" class="col-md-10 control-label">Best Way To Contact You :</label>
    <div class="col-md-12">
        {!! Form::radio('contact_on', 'phone', true) !!} By Phone
        {!! Form::radio('contact_on', 'email', true) !!} By Email
    </div>
</div>
<div class="form-group{{ $errors->has('ref_by') ? ' has-error' : '' }}">
    <label for="ref_by" class="col-md-10 control-label">How did you hear of us? :</label>
    <div class="col-md-12">
        {!! Form::text('ref_by', old('ref_by'), ['class' => 'form-control']) !!}
        @if ($errors->has('ref_by'))
            <span class="help-block">
                <strong>{{ $errors->first('ref_by') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group{{ $errors->has('is_existing') ? ' has-error' : '' }}">
    <label for="ref_by" class="col-md-10 control-label">I am an existing customer :</label>
    <div class="col-md-12">
        {!! Form::checkbox('is_existing',1, false) !!}
        @if ($errors->has('is_existing'))
            <span class="help-block">
                <strong>{{ $errors->first('is_existing') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group">
    <div class="col-md-12">
        <a href="{{ route('delivery.instruction.view') }}" @if (isset($id)) id="handel-previous-{{ $id }}" @else id="handel-previous" @endif class="btn btn-primary pull-left">Previous</a>
        {!! Form::hidden('id', old('id')) !!}
        {!! Form::submit('Next', ['class' => 'btn btn-primary pull-right']) !!}
    </div>
</div>