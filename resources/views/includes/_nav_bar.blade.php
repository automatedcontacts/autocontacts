<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
           <div class="toogle-menu hidden-md hidden-lg">
              <span></span>
              <span></span>
              <span></span> 
           </div>
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ asset('img/meadowspetroleumlogo.png') }}" alt="">
            </a>
        </div>
        <div class="header-right pull-right hidden-xs hidden-sm">
            <ul class="top-link pull-right">
                @if (Auth::user())
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            @if(Auth::user()->is_admin)
                                <li>
                                    <a href="{{ route('admin.home') }}" target="_blank">DashBoard</a>
                                </li>
                            @endif
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
            <!--         <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <ul class="nav navbar-nav">
               <li><a href="#">Home</a></li>
               <li><a href="#">About</a></li>
               <li><a href="#">Contact</a></li>
               <li><a href="#">Service</a></li>
            </ul>
        </div> -->
        </div>
    </div>
</nav>