@extends('layouts.app')

@section('content')
    <div class="panel-heading heading"><h4>Tank Fill Location</h4></div>
    <div class="panel-body row">
        <div class="col-md-12">
            <h5>Select the location that represents where your tank's fill cap is located in relation to the front of your house.</h5>
        </div>
        {!! Form::model($data, ['route' => 'tank.location.save', 'class' => 'form-horizontal']) !!}
            <div class="col-md-6">
                <p><strong>My Tank Fill Location is Located</strong></p>
                <div class="image-box"><img src="{{ asset('img/tanklocation.png') }}" alt="location-image" style="height: 100%; width: 100%"></div>

            </div>
            <div class="col-md-6">
                <div class="form-group{{ $errors->has('tank_location') ? ' has-error' : '' }}">
                    <label for="tank_location" class="col-md-12">My Tank Fill Location is Located:</label>
                    <div class="col-md-12">
                        {!! Form::select('tank_location', [
                            ''          => 'Choose One',
                            '1'         => '1',
                            '2'         => '2',
                            '3'         => '3',
                            '4'         => '4',
                            '5'         => '5',
                            '6'         => '6',
                            '7'         => '7',
                            '8'         => '8',
                            '9'         => '9',
                            '10'         => '10',
                        ], null, ['class' => 'form-control']) !!}
                        @if ($errors->has('tank_location'))
                            <span class="help-block">
                                <strong>{{ $errors->first('tank_location') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('tank_capacity') ? ' has-error' : '' }}">
                    <label for="tank_capacity" class="col-md-12">My Tanks Capacity Is:</label>
                    <div class="col-md-12">
                        {!! Form::select('tank_capacity', $capacities_arr, null, ['class' => 'form-control']) !!}
                        @if ($errors->has('tank_capacity'))
                            <span class="help-block">
                                <strong>{{ $errors->first('tank_capacity') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('current_status') ? ' has-error' : '' }}">
                    <label for="current_status" class="col-md-12">How Much Is In My Tank:</label>
                    <div class="col-md-12">
                         {!! Form::select('current_status', $how_much_arr, null, ['class' => 'form-control']) !!}
                        @if ($errors->has('current_status'))
                            <span class="help-block">
                                <strong>{{ $errors->first('current_status') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <a href="{{ route('fuel.details.show') }}" class="btn btn-primary pull-left">Previous</a>
                    {!! Form::submit('Next', ['class' => 'btn btn-primary pull-right']) !!}
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection