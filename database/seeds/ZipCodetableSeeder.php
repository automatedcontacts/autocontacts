<?php

use App\ZipCode;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ZipCodetableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $i = 0;

        while ($i <= 100) {
        	
        	ZipCode::create([
        		'name'	=> $faker->city,
        		'code'	=> $faker->postcode
        	]);

        	$i++;
        }
    }
}
