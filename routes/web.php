<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@showHomePage')->name('landing.page');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Front end routes
Route::get('/fuel-details', 'FuelsController@showFuelDetailsPage')->name('fuel.details.show');
Route::post('/fuel-details', 'FuelsController@storeFuelDetails')->name('fuel.details.save');
Route::get('/tank-location', 'FuelsController@showTankLocationPage')->name('tank.location.view');
Route::post('/tank-location', 'FuelsController@storeTankLocation')->name('tank.location.save');
Route::get('/delivery-instruction', 'FuelsController@showDeliveryInstructionPage')->name('delivery.instruction.view');
Route::post('/delivery-instruction', 'FuelsController@storeDeliveryInstruction')->name('delivery.instruction.save');
Route::get('/user-details', 'FuelsController@showUserDetailsPage')->name('user.data.view');
Route::get('/user-details/{id}', 'FuelsController@showUserDetailsPage')->name('user.data.edit');
Route::post('/user-details', 'FuelsController@storeUserDetails')->name('user.data.save');
Route::post('/user-details-update', 'FuelsController@updateUserDetails')->name('user.data.update');
Route::get('/services', 'FuelsController@showServicesPage')->name('services.view');
Route::post('/services', 'FuelsController@storeServices')->name('services.save');
Route::any('/place_order/{id}', 'FuelsController@storeOrder')->name('place_order');

// CheckOut routes
Route::get('/checkout', 'PaymentController@showCheckoutPage')->name('user.checkout.view');
Route::get('/process-payment/{token}/{order_id}', 'PaymentController@chargeCard')->name('user.checkout.pay');

// Admin Routes 
Route::namespace('Admin')->middleware('admin')->prefix('admin')->group(function () {
	Route::get('/', 'DashBoardController@showDashBoard')->name('admin.home');

	Route::get('/services', 'ServicesController@servicesList')->name('admin.services.list');
	Route::post('/services-add', 'ServicesController@addService')->name('admin.services.add');
	Route::get('/services-edit/{id}', 'ServicesController@editService')->name('admin.services.edit');
	Route::post('/services-edit', 'ServicesController@updateService')->name('admin.services.update');

	Route::get('/fuels', 'FuelController@fuelsList')->name('admin.fuel.list');
	Route::post('/fuels-add', 'FuelController@addFuel')->name('admin.fuel.add');
	Route::get('/fuels-edit/{id}', 'FuelController@editFuel')->name('admin.fuel.edit');
	Route::post('/fuels-edit', 'FuelController@updateFuel')->name('admin.fuel.update');

	Route::get('/zip-codes', 'ZipCodeController@zipCodeList')->name('admin.zip.code.list');
	Route::post('/zip-code-add', 'ZipCodeController@addZipCode')->name('admin.zip.code.add');
	Route::get('/zip-code-edit/{id}', 'ZipCodeController@editZipCode')->name('admin.zip.code.edit');
	Route::post('/zip-code-edit', 'ZipCodeController@updateZipCode')->name('admin.zip.code.update');

	Route::any('/admin-tank-capacity-list', 'CapacityController@index')->name('admin.tank.capacity.list');
	Route::any('/admin-capacity-add', 'CapacityController@create')->name('admin.capacity.add');
	Route::any('/capacity-edit/{id}', 'CapacityController@edit')->name('admin.capacity.edit');
	Route::post('/admin-capacity-update','CapacityController@update')->name('admin.capacity.update');


	Route::any('/admin-tank-how_much-list','HowMuchTankController@index')->name('admin.tank.how_much.list');
	Route::post('/admin-how_much-add','HowMuchTankController@create')->name('admin.how_much.add');
	Route::any('/how_much-edit/{id}','HowMuchTankController@edit')->name('admin.how_much.edit'); 
	Route::any('/admin-how_much-update','HowMuchTankController@update')->name('admin.how_much.update');    

	Route::get('/users', 'UsersController@usersList')->name('admin.user.list');
	Route::post('/user-add', 'UsersController@addUser')->name('admin.user.add');
	Route::get('/user-details/{id}', 'UsersController@userDetails')->name('admin.user.edit');
	Route::post('/user-edit', 'UsersController@updateUser')->name('admin.user.update');

	Route::get('/orders', 'UsersController@paymentList')->name('admin.order.list');
	Route::get('/incomplete-orders', 'UsersController@orderLists')->name('admin.order.incomplete');
	Route::get('/orders-detail/{id}', 'UsersController@orderDetails')->name('admin.order.details');
});

// // demo email routes
// Route::get('/email-to-user', 'HomeController@emailToUser');
// Route::get('/email-to-admin', 'HomeController@emailToAdmin');